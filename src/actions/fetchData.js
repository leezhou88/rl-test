import axios from 'react-native-axios';
import { 
    FETCH_DATA_LOADING, 
    FETCH_DATA_SUCCESSFUL, 
    FETCH_DATA_FAILED,
} from "../constants"

export const fetchDataFromAPIService = () => {
    return dispatch => {
        dispatch(fetchDataLoading());
        axios.post('http://www.mocky.io/v2/5e6b7c042d000092008e8fbf', {
        headers: {
            'Authorization': 'Basic Y2xpZW50OnNlY3JldA==',
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        })
        .then(response => {
            dispatch(fetchDataSuccessful(response.data));
        })
        .catch(error => {
            dispatch(fetchDataFailed(error));
        });
    };
};

export const fetchDataLoading = () => ({
    type: FETCH_DATA_LOADING
});

export const fetchDataFailed = (error) => ({
    type: FETCH_DATA_FAILED,
    error
});

export const fetchDataSuccessful = (data) => ({
    type: FETCH_DATA_SUCCESSFUL,
    fetchedData: data
});