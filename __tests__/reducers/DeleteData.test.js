import deleteDataReducer from '../../src/reducers/deleteDataReducer'
import * as types from '../../src/constants'

describe('delete data reducer', () => {
  it('should return the initial state', () => {
    expect(deleteDataReducer(undefined, {})).toEqual({
        deleteDataLoading: false,
        deleteSuccessful: false,
        deleteDataFailed: false,
    })
  })

  it('should handle DELETE_DATA_SUCCESSFUL', () => {
    expect(
        deleteDataReducer({
        deleteDataLoading: false,
        deleteSuccessful: false,
        deleteDataFailed: false,
    }, {
        type: types.DELETE_DATA_SUCCESSFUL,
      })
    ).toEqual({
        deleteDataLoading: false,
        deleteSuccessful: true,
        deleteDataFailed: false,
    })
  })
})