import React from 'react'
import { Scene, Router, Actions } from 'react-native-router-flux';
import TabView from './TabView';
import LoginForm from './components/LoginForm';

export default RouterComponent = () => {
    return (
      <Router>
        <Scene key="root" >
            <Scene key="loginForm" component={LoginForm} title='Welcome Goss Media'/>
            <Scene key="tabView" component={TabView} hideNavBar={true}/>
        </Scene>
      </Router>
    )
}