Roughly spent 4 hours to build this basic React Redux framework App,  
I intend to use Firebase for user auth, only provided sample code (as not enough time to registrate and create credential stuff). 
Usage:
Just run expo start, it should work, then scan the QR code with your phone
Just click login button, no need to enter any username password stuff,  action -> reducer will give a fake logged in "User" to display on List view header.  
In the list view,  say it is an Olympic game board, so I called a fake API here to retrieve a list of data then sorted each country by medal QTY and display on list,  swipe the cell to the left,  there will be a delete button, once delete button got clicked,  action will be called to post request to another endpoint (in real world Database would be updated, so the call back will give new data).
There also a meterial UI integrated TabView, say in future, there are more page components can be added etc.
As for TDD,  really no time to impl,  however, if you wish, we can do it pair programing to develop test part
in next step Interview if you like my code :)
Thanks for reading