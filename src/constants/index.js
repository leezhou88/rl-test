export const DELETE_DATA_LOADING = 'delete_country_loading'
export const DELETE_DATA_SUCCESSFUL = 'delete_country_successful'
export const DELETE_DATA_FAILED = 'delete_country_failed'

export const FETCH_DATA_LOADING = 'fetch_country_loading'
export const FETCH_DATA_SUCCESSFUL = 'fetch_country_successful'
export const FETCH_DATA_FAILED = 'fetch_country_failed'

export const EMAIL_CHANGED = 'email_changed'
export const PASSWORD_CHANGED = 'password_changed'
export const LOGIN_USER_SUCCESS = 'login_user_success'
export const LOGIN_USER_FAIL = 'login_user_fail'
export const LOGIN_USER = 'login_user'

export const COUNTRY_STRING = 'country'
export const GOLD_STRING = 'gold'
export const SILVER_STRING = 'silver'
export const BRONZE_STRING = 'bronze'


export const LOGIN_LABEL = 'Login'
export const USER_NAME_PLACEHOLDER = 'Please enter user name'
export const EMAIL_LABLE = 'Email'
export const PASSWORD_LABLE = 'Password'
export const EMAIL_PLACE_HOLDER = 'Please enter email address'
