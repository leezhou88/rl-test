import authReducer from '../../src/reducers/authReducer'
import * as types from '../../src/constants'

describe('auth reducer', () => {
  it('should return the initial state', () => {
    expect(authReducer(undefined, {})).toEqual({
      email: '',
      password: '',
      user: '',
      error: '',
      loading: false
    })
  })

  it('should handle LOGIN_USER_SUCCESS', () => {
    expect(
      authReducer({email: '',
      password: '',
      user: '',
      error: '',
      loading: false
    }, {
        type: types.LOGIN_USER_SUCCESS,
        payload: 'Tester'
      })
    ).toEqual({
      email: '',
      password: '',
      user: 'Tester',
      error: '',
      loading: false
    })
  })
})