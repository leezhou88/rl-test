import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Card, CardSection } from './common';

export default class Placeholder extends Component {
  render() {
    return (
      <Card style={styles.containerStyle}>
        <CardSection>
          <View>
            <Text>
              placeholder
            </Text>
          </View>
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

