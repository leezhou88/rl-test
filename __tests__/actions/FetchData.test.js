import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../src/actions'
import * as types from '../../src/constants'
import fetchMock from 'fetch-mock'
import expect from 'expect'
import axios from 'react-native-axios';

const middlewares = [thunk]
const mockStore = configureMockStore(middlewares)

describe('async actions', () => {
  afterEach(() => {
    fetchMock.restore()
  })

  it('creates FETCH_DATA_SUCCESSFUL when fetching has been done', () => {
    fetchMock.getOnce('/fetching', {
        headers: {
            'Authorization': 'Basic Y2xpZW50OnNlY3JldA==',
            'Content-Type': 'application/x-www-form-urlencoded'
            }
    })

    const expectedActions = [
      { type: types.FETCH_DATA_LOADING },
      { type: types.FETCH_DATA_SUCCESSFUL, fetchedData: [
        {
        "id": 0,
        "country": "uk",
        "gold": 10,
        "silver": 3,
        "bronze": 5
        }
      ]}
    ]
    const store = mockStore({ fetchedData: [
      {
      "id": 0,
      "country": "uk",
      "gold": 10,
      "silver": 3,
      "bronze": 5
      }
    ]})

    // const data = {};
    // axios.get.mockImplementationOnce(() => Promise.resolve(data));
    // await expect(actions.fetchDataFromAPIService()).resolves.toEqual(data);
    // expect(axios.get).toHaveBeenCalledWith(
    //   `http://www.mocky.io/v2/5e6b7c042d000092008e8fbf`,
    // );

    return store.dispatch(actions.fetchDataFromAPIService()).then(() => {      
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})