import { 
    FETCH_DATA_LOADING, 
    FETCH_DATA_SUCCESSFUL, 
    FETCH_DATA_FAILED 
} from "../constants"

const INITIAL_STATE = {
    fetchDataLoading: false,
    fetchedData: [],
    fetchDataFailed: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case FETCH_DATA_LOADING:
            return { ...state, fetchDataLoading: true, fetchDataFailed: false};
        case FETCH_DATA_SUCCESSFUL:
            return { ...state, fetchedData: action.fetchedData, fetchDataLoading: false, fetchDataFailed: false };
        case FETCH_DATA_FAILED:
            return { ...state, fetchDataLoading: false, fetchDataFailed: true };
        default:
            return state;
    }
}

