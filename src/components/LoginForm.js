import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text } from 'react-native';
import { Card, CardSection, Input, Button, Spinner } from './common';
import {
  emailChanged,
  passwordChanged,
  loginUser,
 } from '../actions';
import * as constants from '../constants';

class LoginForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  onButtonPress() {
    const { email, password } = this.props;
    console.log(`email password ${email}`);
    this.props.loginUser({ email, password });
  }

  renderError() {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.errorTextStyle}>
            {this.props.error}
          </Text>
        </View>
      );
    }
  }

  renderButton() {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (
      <Button onPress={this.onButtonPress.bind(this)}>
        {constants.LOGIN_LABEL}
      </Button>
    );
  }

  render() {
    return (
      <Card style={styles.containerStyle}>
        <CardSection>
          <Input
            label={constants.EMAIL_LABLE}
            placeholder={constants.USER_NAME_PLACEHOLDER}
            onChangeText={this.onEmailChange.bind(this)}            
          />
        </CardSection>

        <CardSection>
          <Input
            secureTextEntry
            label={constants.PASSWORD_LABLE}
            placeholder={constants.EMAIL_PLACE_HOLDER}
            onChangeText={this.onPasswordChange.bind(this)}            
          />
        </CardSection>

        {this.renderError()}

        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

const mapStateToProps = ({ authReducer }) => {
  const { email, password, error, loading } = authReducer;
  return { email, password, error, loading };
};

export default connect(mapStateToProps, {
  emailChanged,
  passwordChanged,
  loginUser
})(LoginForm);
