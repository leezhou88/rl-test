import { 
  DELETE_DATA_LOADING, 
  DELETE_DATA_SUCCESSFUL,
  DELETE_DATA_FAILED
} from "../constants"


const INITIAL_STATE = {
  deleteDataLoading: false,
  deleteSuccessful: false,
  deleteDataFailed: false,
};

export default (state = INITIAL_STATE, action) => {
  debugger
  switch (action.type) {
    case DELETE_DATA_LOADING:
      return { ...state, deleteDataLoading: true, deleteSuccessful: false, deleteDataFailed: false };
    case DELETE_DATA_SUCCESSFUL:
      return { ...state, deleteDataLoading: false, deleteSuccessful: true, deleteDataFailed: false };
    case DELETE_DATA_FAILED:
      return { ...state, deleteDataLoading: false, deleteSuccessful: false, deleteDataFailed: true  };
    default:
      return state;
  }
};
