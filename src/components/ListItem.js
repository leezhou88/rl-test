import React, { Component } from 'react';
import { Text } from 'react-native';
import { MedalDetailSection } from './common';
import { COUNTRY_STRING, GOLD_STRING, SILVER_STRING, BRONZE_STRING } from "../constants"

class ListItem extends Component {
  render() {
    const { id, country, gold, silver, bronze } = this.props.country
    return (
      <MedalDetailSection>
        <Text>{COUNTRY_STRING} {country} </Text>
        <Text>{GOLD_STRING} {gold} </Text>
        <Text>{SILVER_STRING} {silver} </Text>
        <Text>{BRONZE_STRING} {bronze}</Text>
      </MedalDetailSection>
    )
  }
}

export default ListItem
