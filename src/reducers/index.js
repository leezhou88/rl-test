import { combineReducers } from 'redux';
import AuthReducer from './authReducer'
import FetchDataReducer from './fetchDataReducer'
import DeleteDataReducer from './deleteDataReducer'

export default combineReducers({
  authReducer: AuthReducer,
  fetchDataReducer: FetchDataReducer,
  deleteDataReducer: DeleteDataReducer
})
