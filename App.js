
import React from 'react';
import { View } from 'react-native';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import reducers from './src/reducers';
import { Header } from './src/components/common';
import Router from './src/Router'
// import firebase from 'react-native-firebase'

const App = () => {
  //Below is the firebase setup, once initialized, it is able to call through out whole app, it is an Oauth 
  //framework impl by Google, many service integrated, such as auth, graphyQL and manymore
  // const config = {
  //   apiKey: 'AIzaSyA404C9kyRQQmH_taxiiklzO4NphxolOfc',
  //   authDomain: 'myapp-51c6d.firebaseapp.com',
  //   databaseURL: 'https://myapp-51c6d.firebaseio.com',
  //   projectId: 'myapp-51c6d',
  //   storageBucket: 'myapp-51c6d.appspot.com',
  //   messagingSenderId: '691179079799'
  // };
  // firebase.initializeApp(config);
  
  const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
  return (    
    <Provider store={store}>
      <Router />
    </Provider>
  );
};

export default App;
