import { Actions } from 'react-native-router-flux';
import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER
} from '../constants';
// import firebase from 'react-native-firebase';

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};

export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });
  
    //Below is an example of using firebase framework to do login
    // firebase.auth().signInWithEmailAndPassword(email, password)
    //   .then(user => loginUserSuccess(dispatch))
    //   .catch(() => {
    //     firebase.auth().createUserWithEmailAndPassword(email, password)
    //       .then(user => loginUserSuccess(dispatch, user))
    //       .catch(() => loginUserFail(dispatch));
    //   });

    //Below is our current app logged in user mock, you do not need to enter at login page, I just mocked it here anyway
    const user = 'Tester';
    const password = 'Password';
    debugger
    if (password === 'Password' && user === 'Tester') {
      loginUserSuccess(dispatch, user);
    } else {
      loginUserFail(dispatch);
    }
  };
};

const loginUserFail = (dispatch) => {
  dispatch({ 
    type: LOGIN_USER_FAIL, 
    error: 'Something went wrong'
  });
};

const loginUserSuccess = (dispatch, user) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: user
  });

  Actions.tabView();
};
