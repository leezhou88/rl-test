import React, { Component } from 'react';
import { View } from 'react-native'
import { connect } from 'react-redux';
import BottomNavigation, {
  IconTab,
} from 'react-native-material-bottom-navigation'
import Icon from '@expo/vector-icons/MaterialCommunityIcons'
import HookSample from './components/Placeholder'
import OlympicGameList from './components/OlympicGameList'
import { Header } from './components/common';

class TabView extends Component {
  state = {
    activeTab: 'OlympicGameList'
  }

  tabs = [
    {
      key: 'OlympicGameList',
      label: 'OlympicGameList',
      barColor: '#00695C',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'movie'
    },
    {
      key: 'HookSample',
      label: 'HookSample',
      barColor: '#388E3C',
      pressColor: 'rgba(255, 255, 255, 0.16)',
      icon: 'gamepad-variant'
    }, 
  ]

  state = {
    activeTab: this.tabs[0].key
  }

  renderIcon = icon => ({ isActive }) => (
    <Icon size={24} color="white" name={icon} />
  )

  renderTab = ({ tab, isActive }) => (
    <IconTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )

  renderSwitch = (param) => {
    switch(param) {
      case 'OlympicGameList':
        return (
          <View style={{ flex: 1 }}>
            <Header headerText={this.props.user} />
            <OlympicGameList />
          </View>
        );
      case 'HookSample':
        return <HookSample />;
      default:
        return <OlympicGameList />;
    }
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
         {this.renderSwitch(this.state.activeTab)}      
        <BottomNavigation
          tabs={this.tabs}
          activeTab={this.state.activeTab}
          onTabPress={newTab => this.setState({ activeTab: newTab.key })}
          renderTab={this.renderTab}
          useLayoutAnimation
        />
      </View>
    )
  }
}

const mapStateToProps = ({authReducer}) => {  
  const { user } = authReducer;
  return { user };
};


export default connect(mapStateToProps, null)(TabView);