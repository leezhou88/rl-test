import { 
  DELETE_DATA_LOADING, 
  DELETE_DATA_SUCCESSFUL,
  DELETE_DATA_FAILED
} from "../constants"
import axios from 'react-native-axios';

export const deleteDataAPIService = (itemID) => {
  return dispatch => {
      dispatch(deleteDataLoading());
      //In real life, it should call the update endpoint & pass ID, without expecting new data results coming back, 
      //Only fetchData action should be responsible reading final results.      
      axios.post(`http://www.mocky.io/v2/5e6e70062f00007064a0387d/${itemID}`,
      {
        headers: {
          'Authorization': 'Basic Y2xpZW50OnNlY3JldA==',
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then(response => {
        debugger
          dispatch(deleteDataSuccessful());
      })
      .catch(error => {
          dispatch(deleteDataFailed(error));
      });
  };
};

export const deleteDataLoading = () => ({
  type: DELETE_DATA_LOADING
});

export const deleteDataFailed = (error) => ({
  type: DELETE_DATA_FAILED,
  error
});

export const deleteDataSuccessful = () => ({
  type: DELETE_DATA_SUCCESSFUL
});