import React, { Component } from 'react';
import { FlatList, View } from 'react-native';
import { connect } from 'react-redux';
import ListItem from './ListItem'
import {Spinner} from './common/Spinner'
import Swipeout from 'react-native-swipeout';
import * as actions from '../actions';


class CountriesList extends Component {
  constructor(props) {
    super(props);

    this.props.fetchData();
    
  }

  sortArrayDesc(obj, key) {
    return obj.sort(function (r1,r2) {
      return r2.gold < r1.gold ? -1
           : r2.gold > r1.gold ? 1
           : 0
    })
  }

  renderRow(country) {
    let swipeBtns = [{
      text: 'Delete',
      backgroundColor: 'red',
      onPress: () => { this.props.deleteData(country.id) }
      }
    ];

    return (
      <Swipeout right={swipeBtns}
        autoClose='true'>
        <ListItem country={country} />
      </Swipeout>
    )
  }

  render() {       
    debugger
    if(this.props.fetchDataLoading || this.props.deleteDataLoading) {           
      return (
        <Spinner size="large" />
      );
    } else if(this.props.fetchDataFailed || this.props.deleteDataFailed){
      return (
        //error view, omitted for this test...
        <View>
        </View>
      )
    } else if(this.props.deleteDataSuccessful){
      return (
        <View>
          <Text>Mock deleted succesful</Text>
        </View>
      )
    } else {
      return (        
        <FlatList
          data={this.sortArrayDesc(this.props.fetchedData, 'gold')}
          renderItem={({item}) => this.renderRow(item)}
          keyExtractor={(item, index) => item.id}
        />
      );
    }
    
  }
}

const mapStateToProps = ({fetchDataReducer, deleteDataReducer}) => {  
  const { fetchDataLoading, fetchedData, fetchDataFailed } = fetchDataReducer;
  const { deleteDataLoading, deleteDataSuccessful, deleteDataFailed } = deleteDataReducer;
  return { fetchDataLoading, fetchedData, fetchDataFailed, deleteDataLoading, deleteDataSuccessful, deleteDataFailed };
};

const mapDispatchToProps = (dispatch) => ({
    deleteData: (countryId) => dispatch(actions.deleteDataAPIService(countryId)),
    fetchData: () => dispatch(actions.fetchDataFromAPIService()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CountriesList);
